# fabukousweather
AngularJS 1.5 ES6 and Material

## Instructions:
After cloning this project in order to run it, you need to get into the directory and execute the following commands :
```
npm install
gulp
```

## To test the code execute :
```
gulp build 
gulp test
```


### That's all folks! Happy trails!
