var gulp          = require('gulp');
var notify        = require('gulp-notify');
var source        = require('vinyl-source-stream');
var browserify    = require('browserify');
var babelify      = require('babelify');
var ngAnnotate    = require('browserify-ngannotate');
var browserSync   = require('browser-sync').create();
var rename        = require('gulp-rename');
var templateCache = require('gulp-angular-templatecache');
var uglify        = require('gulp-uglify');
var merge         = require('merge-stream');
var sass          = require('gulp-sass');
var rename        = require('gulp-rename');
var minifycss     = require('gulp-minify-css');
var concat        = require('gulp-concat');
var Server        = require('karma').Server;

// Where our files are located
var jsFiles   = "src/js/**/*.js";
var viewFiles = "src/js/**/*.html";
var sassFiles = "./src/assets/sass/**/*.scss";
var interceptErrors = function(error) {
  var args = Array.prototype.slice.call(arguments);

  // Send error to notification center with gulp-notify
  notify.onError({
    title: 'Compile Error',
    message: '<%= error.message %>'
  }).apply(this, args);

  // Keep gulp from hanging on this task
  this.emit('end');
};

gulp.task("testwatch", function(done) {
  new Server({
    configFile: __dirname + '/test/karma.conf.js',
    singleRun: false
  }, done).start();
});

gulp.task("test", function(done) {
  new Server({
    configFile: __dirname + '/test/karma.report.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task("sass", function() {

  // putting font files where they are needed
  gulp.src('./node_modules/weather-icons/font/**/*')
  .pipe(gulp.dest('./build/font/'));

  // copying css files to build
  gulp.src('./node_modules/weather-icons/css/weather-icons.min.css')
  .pipe(gulp.dest('./build/css/'));

  gulp.src('./node_modules/angular-google-places-autocomplete/dist/autocomplete.min.css')
  .pipe(gulp.dest('./build/css/'));

  // process our custom css
  gulp.src('./src/assets/sass/main.scss')
	.pipe(sass({ style: 'expanded' }))
	.pipe(gulp.dest("./src/assets/css"))
	.pipe(rename({suffix: '.min'}))
	.pipe(minifycss())
	.pipe(gulp.dest('./build/css/'));

});

gulp.task('browserify', ['views'], function() {
  return browserify('./src/js/app.js')
      .transform(babelify, {presets: ["es2015"]})
      .transform(ngAnnotate)
      .bundle()
      .on('error', interceptErrors)
      //Pass desired output filename to vinyl-source-stream
      .pipe(source('main.js'))
      // Start piping stream to tasks!
      .pipe(gulp.dest('./build/'));
});

gulp.task('html', function() {
  return gulp.src("src/index.html")
      .on('error', interceptErrors)
      .pipe(gulp.dest('./build/'));
});

gulp.task('views', function() {
  return gulp.src(viewFiles)
      .pipe(templateCache({
        standalone: true
      }))
      .on('error', interceptErrors)
      .pipe(rename("app.templates.js"))
      .pipe(gulp.dest('./src/js/config/'));
});

// This task is used for building production ready
// minified JS/CSS files into the dist/ folder
gulp.task('build', ['html', 'browserify', 'sass'], function() {
  var html = gulp.src("build/index.html")
                 .pipe(gulp.dest('./dist/'));

  var js = gulp.src("build/main.js")
               .pipe(uglify())
               .pipe(gulp.dest('./dist/'));

  return merge(html,js);
});

gulp.task('default', ['html', 'browserify'], function() {

  browserSync.init(['./build/**/**.**'], {
    server: "./build",
    port: 4000,
    notify: false,
    ui: {
      port: 4001
    }
  });

  // to update css
  gulp.watch(sassFiles, ["sass"]);

  gulp.watch("src/index.html", ['html']);
  gulp.watch(viewFiles, ['views']);
  gulp.watch(jsFiles, ['browserify']);

});
