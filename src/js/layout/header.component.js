class AppHeaderCtrl {
  constructor(AppConstants, weatherService) {
    'ngInject';

    this.appName = AppConstants.appName;
    this.weatherService = weatherService;
  }
  settingsClick () {
    this.weatherService.resetDefault();
  }
}

let AppHeader = {
  controller: AppHeaderCtrl,
  templateUrl: 'layout/header.html'
};

export default AppHeader;
