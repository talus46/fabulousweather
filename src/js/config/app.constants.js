const AppConstants = {
  yqlend: 'https://query.yahooapis.com/v1/public/yql',
  appName: 'Fabulous Weather',
};

export default AppConstants;
