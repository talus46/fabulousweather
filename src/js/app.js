import angular from 'angular';

import constants  from './config/app.constants';
import appConfig  from './config/app.config';
import appRun     from './config/app.run';

import 'angular-ui-router';
import 'angular-messages';
import 'angular-google-places-autocomplete';
import 'angular-aria';
import 'angular-animate';
import 'angular-material';

import './config/app.templates';

// our custom code
import './services';
import './layout';
import './components';
import './home';

const requires = [
  'ui.router',
  'ngMaterial',
  'google.places',
  'templates',
  'app.services',
  'app.layout',
  'app.components',
  'app.home'
];

window.app = angular.module('app', requires);

angular.module('app').constant('AppConstants', constants);

angular.module('app').config(appConfig);

angular.module('app').run(appRun);

angular.bootstrap(document, ['app'], {
  strictDi: true
});
