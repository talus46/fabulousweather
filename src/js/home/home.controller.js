class HomeCtrl {

  constructor(AppConstants, weatherService) {
    'ngInject';

    this.appName = AppConstants.appName ;
    this.weatherService = weatherService;

    this.address = "";

    this.checkSettingsState();

    if(!this.showSettings) {
      this.weatherService.getDefaultWeather( this.loadWeather.bind(this));
    }
    this.weatherService.addResetHook(() => { this.showSettings = true; });
  }

  checkSettingsState () {

    this.showSettings = (this.weatherService.defaultCity === null) ;

  }

  loadWeather (wData) {

    // today;
    this.weatherInfo = wData[0];
    this.wForecast = [];

    // select the next 5 days to show
    let i = 1;

    while(this.wForecast.length < 5) {
      this.wForecast.push(wData[i]);
      i++;
    }

  }

  clickApply (ev) {

    for ( let i = 0 ; i < this.address.address_components.length; i++ ) {
      let item = this.address.address_components[i];

      if (item.types.indexOf('locality') > -1) {
        this.weatherService.setDefaultCity(item.long_name);
      }

      if (item.types.indexOf('country') > -1) {
        this.weatherService.setDefaultCountry(item.short_name);
      }

      if (this.weatherService.defaultCity !== null && this.weatherService.defaultCountry !== null) {
        this.checkSettingsState();
        this.weatherService.getDefaultWeather( this.loadWeather.bind(this));
        break;
      }

    }

  }

}

export default HomeCtrl;
