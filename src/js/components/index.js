import angular from 'angular';

let componentsModule = angular.module('app.components', []);

import WeatherTile from './weathertile.component';
componentsModule.component('weatherTile', WeatherTile);

export default componentsModule;
