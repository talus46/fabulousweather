class WeatherTileCtrl {
  constructor(AppConstants) {
    'ngInject';

    this.appName = AppConstants.appName;
    this.size = 'large';

  }
}

let WeatherTile = {
  controller: WeatherTile,
  templateUrl: 'components/weathertile.html',
  bindings :{
    'winfo':'<',
    'size':'<',
    'wcss':'&',
  }
};

export default WeatherTile;
