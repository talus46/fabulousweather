import angular from 'angular';
import WeatherService from './weather-service';

// Create the module where our functionality can attach to
let servicesModule = angular.module('app.services', []);

servicesModule.service('weatherService', WeatherService);

export default servicesModule;
