const isNil = (val) => {
  return (val === null || val === undefined);
}

class  WeatherService {

  constructor (AppConstants,$http, $window) {
    'ngInject';
     this.units = 'metric';
     this.http = $http;
     this.YQLURL  = AppConstants.yqlend;
     this.wcss = {
       '0': 'tornado',
       '1': 'day-storm-showers',
       '2': 'hurricane',
       '3': 'thunderstorm',
       '4': 'thunderstorm',
       '5': 'rain-mix',
       '6': 'rain-mix',
       '7': 'rain-mix',
       '8': 'hail',
       '9': 'showers',
       '10': 'hail',
       '11': 'showers',
       '12': 'showers',
       '13': 'snow',
       '14': 'day-snow',
       '15': 'snow-wind',
       '16': 'snow',
       '17': 'hail',
       '18': 'rain-mix',
       '19': 'dust',
       '20': 'fog',
       '21': 'windy',
       '22': 'smoke',
       '23': 'strong-wind',
       '24': 'strong-wind',
       '25': 'snowflake-cold',
       '26': 'cloudy',
       '27': 'night-cloudy',
       '28': 'day-cloudy',
       '29': 'night-cloudy',
       '30': 'day-cloudy',
       '31': 'night-clear',
       '32': 'day-sunny',
       '33': 'night-partly-cloudy',
       '34': 'day-sunny-overcast',
       '35': 'rain-mix',
       '36': 'hot',
       '37': 'day-storm-showers',
       '38': 'day-storm-showers',
       '39': 'day-storm-showers',
       '40': 'showers',
       '41': 'snow-wind',
       '42': 'snow',
       '43': 'snow-wind',
       '44': 'day-sunny-overcast',
       '45': 'day-storm-showers',
       '46': 'snow',
       '47': 'day-storm-showers',
       '3200': 'stars'
     };

     this.localStorage = $window.localStorage;
     this.resetFlag = false;
     this.resetHooks = [];

     this.loadDefaultFromStorage();
  }

  addResetHook (fn) {
    this.resetHooks.push(fn);
  }

  resetDefault () {
    this.resetFlag = true;
    this.resetHooks.map((fn) => { fn(true);});
  }

  loadDefaultFromStorage () {
    this.defaultCountry = this.localStorage.getItem("defaultCountry");
    this.defaultCity = this.localStorage.getItem("defaultCity");
  }

  setDefaultCity (city) {
    this.defaultCity = city;

    this.localStorage.setItem ("defaultCity", city);
  }

  setDefaultCountry (country) {
    this.defaultCountry = country;

    this.localStorage.setItem("defaultCountry",country);
  }

  getYQueryWeatherforCity (country_code, city_name) {
    const city = city_name.toLowerCase(city_name);
    const country = country_code.toLowerCase(country_code);
    const un = (this.units === 'metric') ? 'c':'f';

    return `select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="${city}, ${country}" ) and u="${un}"`;

  }

  filterForecast (rawData) {

    let rv  = JSON.parse(JSON.stringify( rawData.item.forecast) );
    let title = rawData.title.split(" - ")[1];
    let icons = this.wcss;

    // adding details to the current day:
    for ( let [k, v] of Object.entries(rawData.item.condition) ) {
      rv[0][k] = v;
    }

    return rv.map ((item) => {
      item.units = rawData.units;
      item.title = title;
      item.icon = "wi-"+icons[item.code];
      return item;
    });
  }

  getDefaultWeather (requestHandler) {

    if(isNil(this.defaultCity) || isNil(this.defaultCountry)) {

      if ( !isNil(requestHandler)) {
        requestHandler(false);
      }

    } else {

      this.getWeather (this.defaultCountry, this.defaultCity, requestHandler);

    }
  }

  getWeather (country_code, city_name, requestHandler) {

    const wQuery = this.getYQueryWeatherforCity(country_code, city_name);
    const url = this.YQLURL + "?q=" + encodeURI(wQuery) + "&format=json";
    let self  = this;

    this.http.get(url)
    .then(
      (resp) => {
        if ( !isNil (requestHandler)) {
          requestHandler( ( (isNil (resp.data.query.results) || isNil (resp.data.query.results.channel)) ? false :this.filterForecast (resp.data.query.results.channel) ) );
        }
      },
      (resp) => {
        if ( !isNil (requestHandler)) {
          requestHandler (false);
        }
      }
    );
  }

}

export default WeatherService;
