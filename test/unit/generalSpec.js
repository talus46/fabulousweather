
describe ('Fabulous Weather Unit tests', function() {
  var $controller, $rootscope, createWeatherService, weatherService;
  var AppConstants =  {
    yqlend: 'https://query.yahooapis.com/v1/public/yql',
    appName: 'Test Fabulous Weather'
  };
  beforeEach ( module('app') );

  beforeEach ( inject ( function ( _$controller_, _$rootScope_, _weatherService_ ) {
    $controller = _$controller_;
    $rootscope = _$rootScope_;
    weatherService = _weatherService_;
  }) );

  describe ( 'Home Controller Section', function () {
    it ( 'home appName', function () {
      var ctrl = $controller( 'HomeCtrl', {
        'AppConstants' : AppConstants,
        'weatherService': weatherService} );
      expect(ctrl.appName).toEqual('Test Fabulous Weather');
    });

    it ( 'Home Controller showSettings initial status', function() {
      var ctrl = $controller('HomeCtrl', {
        'AppConstants':AppConstants,
        'weatherService': weatherService} );
        expect ( ctrl.showSettings ).toEqual ( true );
    });

  });

  describe ( 'weatherService Section', function () {

    it ( 'initial defaultCity', function () {
      expect( weatherService.defaultCity ).toEqual ( null );
    });

    it ( 'initial defaultCountry', function () {
      expect( weatherService.defaultCountry ).toEqual ( null );
    });

    it ( 'weather YQL', function () {
      var yql = weatherService.getYQueryWeatherforCity ( 'AU', 'sydney' );
      expect( yql ).toEqual( 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="sydney, au" ) and u="c"' );
    });

    it ( 'weather request', function () {
      var executedHook = false;
      weatherService.addResetHook(function () { executedHook = true; });
      weatherService.resetDefault();
      weatherService.getWeather('AU',  'sydney', function(data) {
          expect( data ).not.toEqual( false );
      });
    });



  });
});
